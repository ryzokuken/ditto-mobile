const dittoPurple = '#7817B4'
const darkAccent = '#151129'
const veryLight = '#E9E2F2'
const transparentLight = '#E9E2F270'

// DARK Theme
const theme = {
  dittoPurple,
  backgroundColor: '#000',
  chromeColor: darkAccent,
  primaryTextColor: veryLight,
  secondaryTextColor: transparentLight,
  primaryAccentColor: '#E90F9F',
  secondaryAccentColor: '#0089FF',
  darkAccentColor: darkAccent,
  lightAccentColor: veryLight,
  iconColor: veryLight,
  errorRed: '#B51C4B',
  chat: {
    otherBubbleBackground: darkAccent,
    myBubbleBackground: dittoPurple,
    composerBorder: '#E9E2F240'
  },
  placeholderTextColor: transparentLight
}
export default theme
