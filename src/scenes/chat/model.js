import { Model, tableSchema } from '@nozbe/watermelondb'
import { children, date, field, immutableRelation, json, relation } from '@nozbe/watermelondb/decorators'

export const chatSchema = tableSchema({
  name: 'chats',
  columns: [
    { name: 'name', type: 'string' },
    { name: 'avatar', type: 'string', isOptional: true },
    { name: 'description', type: 'string', isOptional: true },
    { name: 'status', type: 'string', isIndexed: true },
    { name: 'is_direct', type: 'boolean', isIndexed: true },
    { name: 'is_start', type: 'boolean' },
    { name: 'fully_read', type: 'string', isOptional: true },
    { name: 'tags', type: 'string', isOptional: true },
    { name: 'aliases', type: 'string', isOptional: true },
    { name: 'settings', type: 'string' }
  ]
})

export class ChatModel extends Model {
  static table = 'chats'
  static associations = {
    messages: { type: 'has_many', foreignKey: 'room_id' },
    memberships: { type: 'has_many', foreignKey: 'room_id' },
    receipts: { type: 'has_many', foreignKey: 'room_id' }
  }

  @field('name') name
  @field('avatar') avatar
  @field('description') description
  @field('status') status // MatrixMember membership: invite, join
  @field('is_direct') direct
  @field('is_start') atStart
  @field('fully_read') fullyRead
  @json('tags', json => json) tags
  @json('aliases', json => json) aliases
  @json('settings', json => json) settings // encryption, public/private, guest access, invites rules, push rules, history visibility, power level
  @children('messages') messages
  @children('memberships') memberships
  @children('receipts') receipts
}

export const membershipSchema = tableSchema({
  name: 'memberships',
  columns: [
    { name: 'room_id', type: 'string', isIndexed: true },
    { name: 'user_id', type: 'string' },
    { name: 'state', type: 'string' },
    { name: 'power_level', type: 'number', isOptional: true }
  ]
})

export class MembershipModel extends Model {
  static table = 'memberships'
  static associations = {
    chats: { type: 'belongs_to', key: 'room_id' },
    users: { type: 'belongs_to', key: 'user_id' }
  }

  @immutableRelation('chats', 'room_id') room
  @immutableRelation('users', 'user_id') user
  @field('state') state // join, leave or invite
  @field('power_level') powerLevel
}

export const receiptSchema = tableSchema({
  name: 'receipts',
  columns: [
    { name: 'room_id', type: 'string' },
    { name: 'user_id', type: 'string' },
    { name: 'type', type: 'string' },
    { name: 'message_id', type: 'string', isIndexed: true },
    { name: 'sent_at', type: 'number' }
  ]
})

export class ReceiptModel extends Model {
  static table = 'receipts'
  static associations = {
    chats: { type: 'belongs_to', key: 'room_id' },
    users: { type: 'belongs_to', key: 'user_id' },
    messages: { type: 'belongs_to', key: 'message_id' }
  }

  @immutableRelation('chats', 'room_id') room
  @immutableRelation('users', 'user_id') user
  @field('type') type // read or fullyRead
  @relation('messages', 'message_id') message
  @date('sent_at') sent
}
