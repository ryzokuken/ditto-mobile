import { Q } from '@nozbe/watermelondb'

import matrix from '../../../services/matrix/service'
import messageService from './service'

const debug = require('debug')('ditto:scenes:chat:message:messages')

class Messages {
  getAll () {
    return messageService.getMessages().query().observe()
  }

  getByChat (roomId) {
    return messageService.getMessages().query(Q.where('room_id', roomId)).observe()
  }

  getContent (message, sender, format = 'text') {
    if (!message) return ''
    const { content, type } = message

    switch (type) {
      case 'typing': {
        if (content.length > 1) {
          return `${content.length} users are typing…`
        } else return `${content[0].name} is typing…`
      }
      case 'notice':
      case 'text':
        if (format === 'html' && content.format === 'org.matrix.custom.html') {
          return content.formatted_body
        } else {
          return content.body
        }
      case 'image':
        if (format === 'thumb') {
          // TODO: different sizes in constants or something
          return matrix.getImageUrl(content.url, 250, 180)
        } else if (format === 'full') {
          return matrix.getImageUrl(content.url)
        } else {
          return `${sender.name} sent an image`
        }
      case 'emote':
        return `${sender.name} ${content.body}`
      case 'member.membership':
        switch (content.membership) {
          case 'invite':
            return `${sender.name} invited ${content.user.name} to join the chat`
          case 'join':
            return `${sender.name} joined the chat`
          case 'leave':
            return `${sender.name} left the chat`
          default:
            return `Ditto does not support the membership "${content.membership}" yet`
        }
      case 'member.name':
        return `${content.prev} changed his display name to ${content.new}`
      case 'member.avatar':
        return `${sender.name} changed his avatar`
      case 'room.creation':
        return `${sender.name} created the chat\nHooray!`
      case 'room.name':
        return `${sender.name} changed the chat name to "${content.name}"`
      case 'room.avatar':
        return `${sender.name} changed the chat avatar`
      case 'room.description':
        return `${sender.name} changed the chat description`
      case 'room.encryption':
      case 'room.guestAccess':
      case 'room.historyVisibility':
      case 'room.joinRules':
      case 'room.powerLevels':
        return `${sender.name} changed the chat settings`
      case 'encrypted':
        return 'Ditto does not support encrypted messages yet'
      case 'location':
        return 'Ditto does not support location sharing yet'
      case 'file':
        return 'Ditto does not support file sharing yet'
      case 'audio':
      case 'video':
        return `Ditto does not support ${type} files yet`
      default:
        return `Ditto does not support the type "${type}" yet`
    }
  }

  getCount () {
    return messageService.getMessages().query().observeCount()
  }

  getCountByChat (roomId) {
    return messageService.getMessages().query(Q.where('room_id', roomId)).observeCount()
  }

  getLatestMessage (roomId) {
    return messageService.getLatestMessage(roomId)
  }

  getMessage (eventId) {
    try {
      return messageService.getMessages().findAndObserve(eventId)
    } catch (e) {
      debug('Error finding message %s', eventId, e)
    }
  }

  send (content, type, roomId) {
    matrix.sendMessage(content, type, roomId)
  }

  sortByLastSent (messages) {
    const sorted = [...messages]
    sorted.sort((a, b) => {
      return a.sent < b.sent ? 1 : a.sent > b.sent ? -1 : 0
    })
    return sorted
  }
}

const messages = new Messages()
export default messages
