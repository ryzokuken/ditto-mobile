import { Model, tableSchema } from '@nozbe/watermelondb'
import { children, date, field, immutableRelation, json } from '@nozbe/watermelondb/decorators'

export const messageSchema = tableSchema({
  name: 'messages',
  columns: [
    { name: 'room_id', type: 'string', isIndexed: true },
    { name: 'user_id', type: 'string' },
    { name: 'type', type: 'string' },
    { name: 'content', type: 'string', isOptional: true },
    { name: 'sent_at', type: 'number' },
    { name: 'status', type: 'string' },
    { name: 'is_edited', type: 'boolean' },
    { name: 'latest_update', type: 'string', isOptional: true },
    { name: 'reactions', type: 'string', isOptional: true }
  ]
})

export class MessageModel extends Model {
  static table = 'messages'
  static associations = {
    chats: { type: 'belongs_to', key: 'room_id' },
    users: { type: 'belongs_to', key: 'user_id' },
    receipts: { type: 'has_many', foreignKey: 'message_id' }
  }

  @immutableRelation('chats', 'room_id') room
  @immutableRelation('users', 'user_id') sender
  @field('type') type // room.join, room.leave… message.text, message.image… message.redacted…
  @json('content', json => json) content
  @date('sent_at') sent
  @field('status') status // EventStatus
  @field('is_edited') edited
  @json('latest_update', json => json) latestUpdate // { eventId, timestamp }
  @json('reactions', json => json) reactions
  @children('receipts') receipts
}
