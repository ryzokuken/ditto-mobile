import { Q } from '@nozbe/watermelondb'

import matrix from '../../services/matrix/service'
import auth from '../auth/auth'
import chatService from './service'

// const debug = require('debug')('ditto:scenes:chat:chats')

class Chats {
  chatClosed (roomId) {
    chatService.setAction({
      type: chatService.actions.CHAT_CLOSED,
      payload: roomId
    })
  }

  chatOpened (roomId) {
    chatService.setAction({
      type: chatService.actions.CHAT_OPENED,
      payload: roomId
    })
  }

  async createChat (options) {
    const matrixRoom = await matrix.createRoom(options)
    if (!matrixRoom) return null
    return {
      id: matrixRoom.roomId,
      name: matrixRoom.name
    }
  }

  fetchPreviousMessages (roomId) {
    chatService.setAction({
      type: chatService.actions.CHAT_FETCH_PREVIOUS_HISTORY,
      payload: roomId
    })
  }

  getAvatarUrl (mxcUrl, size) {
    if (mxcUrl == null) return null
    return matrix.getImageUrl(mxcUrl, size, size, 'crop')
  }

  getChat (roomId) {
    return chatService.getChats().findAndObserve(roomId)
  }

  getChatsByDirect (direct = true) {
    return chatService.getChats().query(Q.where('is_direct', direct)).observeWithColumns(['name', 'avatar', 'description', 'status', 'is_start', 'is_direct', 'fully_read'])
  }

  getJoinedMembersCount (roomId) {
    return chatService.getMemberships()
      .query(Q.where('room_id', roomId), Q.where('state', 'join'))
      .observeCount()
  }

  getMembershipsByChat (roomId) {
    return chatService.getMemberships().query(Q.where('room_id', roomId)).observeWithColumns(['state'])
  }

  getMyReadReceipt (receipts) {
    const myId = auth.getUserId()
    for (const receipt of receipts) {
      if (receipt.user.id === myId) {
        return receipt
      }
    }
    return null
  }

  getTyping (roomId) {
    return chatService.getTyping(roomId)
  }

  isChatCreated (roomId) {
    return chatService.getChats().query(Q.where('id', roomId)).observeCount()
  }

  leaveChat (roomId) {
    chatService.setAction({
      type: chatService.actions.CHAT_LEAVE,
      payload: roomId
    })
  }

  sendReadReceipt (roomId, messageId) {
    chatService.setAction({
      type: chatService.actions.RECEIPT_SEND,
      payload: { roomId, messageId }
    })
  }

  isTyping (roomId, isTyping) {
    chatService.setAction({
      type: chatService.actions.CHAT_TYPING,
      payload: { roomId, isTyping }
    })
  }
}

const chats = new Chats()
export default chats
