import { Q } from '@nozbe/watermelondb'
import _ from 'lodash'
import { EventTimeline } from 'matrix-js-sdk'
import { BehaviorSubject, Subject } from 'rxjs'

import matrix from '../../services/matrix/service'
import database from '../../services/storage/database'

const debug = require('debug')('ditto:scenes:chats:service')

const TYPING_TIMEOUT = 10000 // 10s

class ChatService {
  _chats
  _memberships
  _receipts
  _ephemeral
  _local
  _syncList
  _nextSyncList
  _isSyncing
  _action
  actions

  constructor () {
    this._chats = database.getCollection('chats')
    this._memberships = database.getCollection('memberships')
    this._receipts = database.getCollection('receipts')
    this._ephemeral = {}
    this._local = {}
    this._syncList = { chats: {}, memberships: {}, receipts: {} }
    this._nextSyncList = { chats: {}, memberships: {}, receipts: {} }
    this._isSyncing = false
    this._action = new Subject(null)
    this.actions = {
      CHAT_FETCH_PREVIOUS_HISTORY: 'chat/fetchPreviousHistory',
      CHAT_CLOSED: 'chat/closed',
      CHAT_LEAVE: 'chat/leave',
      CHAT_OPENED: 'chat/opened',
      CHAT_TYPING: 'chat/typing',
      RECEIPT_SEND: 'receipt/send'
    }
  }

  init () {
    matrix.getAction().subscribe((action) => {
      switch (action.type) {
        case matrix.actions.EVENT_ROOM_STATE:
          this._handleChatStateEvent(action.payload.event, action.payload.roomState)
          break
        case matrix.actions.EVENT_ROOM_ACCOUNT:
          this._handleChatAccountEvent(action.payload.event, action.payload.room)
          break
        case matrix.actions.EVENT_ROOM_MEMBER:
          this._handleMembershipEvent(action.payload.member)
          break
        case matrix.actions.EVENT_ROOM_RECEIPT:
          this._handleReceiptEvent(action.payload.event, action.payload.room)
          break
        case matrix.actions.EVENT_ROOM_TYPING:
          this._handleTypingEvent(action.payload.event, action.payload.member)
          break
        case matrix.actions.ROOM_START_REACHED:
          this._handleStartReached(action.payload.room)
          break
        default:
      }
    })

    this.getAction().subscribe((action) => {
      switch (action.type) {
        case this.actions.CHAT_CLOSED:
          this._isTyping(action.payload, false)
          break
        case this.actions.CHAT_LEAVE:
          matrix.leaveRoom(action.payload)
          break
        case this.actions.CHAT_FETCH_PREVIOUS_HISTORY:
          matrix.fetchPreviousMessages(action.payload)
          break
        case this.actions.CHAT_TYPING: {
          const { roomId, isTyping, timeout } = action.payload
          this._isTyping(roomId, isTyping, timeout)
          break
        }
        case this.actions.RECEIPT_SEND:
          matrix.sendReadReceipt(action.payload.roomId, action.payload.messageId)
          break
        default:
      }
    })
  }

  getAction () {
    return this._action
  }

  setAction (action) {
    if (!Object.values(this.actions).includes(action.type)) throw Error('Unknown action type:', action)
    // debug('Set action:', action)
    this._action.next(action)
  }

  getChats () {
    return this._chats
  }

  getMemberships () {
    return this._memberships
  }

  getTyping (roomId) {
    if (!this._ephemeral[roomId]) this._ephemeral[roomId] = {}
    if (!this._ephemeral[roomId].typing) this._ephemeral[roomId].typing = new BehaviorSubject([])
    return this._ephemeral[roomId].typing
  }

  isFirstSync () {
    return this._isFirstSync
  }

  //* *******************************************************************************
  // Sync
  //* *******************************************************************************

  finishSync () {
    for (const key in this._syncList) {
      delete this._syncList[key]
    }
    this._syncList = { chats: {}, memberships: {}, receipts: {} }
  }

  pushNextSyncList () {
    _.merge(this._syncList, this._nextSyncList)
    this._nextSyncList = { chats: {}, memberships: {}, receipts: {} }
  }

  setSyncing (isSyncing) {
    this._isSyncing = isSyncing
  }

  async getEmptyAllActions () {
    const actions = []

    try {
      const chats = await this._chats.query().fetch()
      for (const chat of chats) {
        actions.push(chat.prepareDestroyPermanently())
      }

      const memberships = await this._memberships.query().fetch()
      for (const membership of memberships) {
        actions.push(membership.prepareDestroyPermanently())
      }

      const receipts = await this._receipts.query().fetch()
      for (const receipt of receipts) {
        actions.push(receipt.prepareDestroyPermanently())
      }

      return actions
    } catch (e) {
      debug('Error getting empty chat actions:', e)
      return actions
    }
  }

  async getSyncActions (collections) {
    debug('Chat data to sync:', this._syncList, Object.entries(this._syncList.chats))
    let actions = []

    // Chat events
    for (const [roomId, chatEvents] of Object.entries(this._syncList.chats)) {
      let { matrixRoom, events } = chatEvents
      if (!matrixRoom) matrixRoom = matrix.getRoom(roomId)
      try {
        const chat = await this._chats.find(roomId)

        // Remove chat and dependencies if we leave it
        if (chatEvents.leave) {
          const chatActions = await this._getChatRemoveActions(chat)
          actions = [...actions, ...chatActions]
          collections.chats[roomId] = { matrixRoom, leave: true }
          // Fetch users that should be deleted, meaning users that are only in
          // rooms that are about to be deleted
          const matrixMembers = matrixRoom.getLiveTimeline().getState(EventTimeline.FORWARDS).getMembers()
          const matrixUsers = matrixMembers.map(member => member.user)
          for (const matrixUser of matrixUsers) {
            if (matrixUser.userId === matrix.getUserId()) continue
            // Check if that user is in an other room that's not being removed
            const memberships = await this._memberships.query(Q.where('user_id', matrixUser.userId)).fetch()
            let deleteUser = true
            for (let i = 0; i < memberships.length && deleteUser; i++) {
              if (memberships[i].room.id !== roomId &&
                  !(collections.chats[memberships[i].room.id] &&
                  collections.chats[memberships[i].room.id].leave)) {
                deleteUser = false
              }
            }
            if (deleteUser) {
              collections.users[matrixUser.userId] = { action: 'delete', matrixUser }
            }
          }
          continue
        }

        // Update chat
        const updatedChat = await this._getChatUpdateAction(chat, matrixRoom, events)
        if (updatedChat) {
          actions.push(updatedChat)
          collections.chats[roomId] = updatedChat
        }
      } catch (e) {
        // Create new chat
        // We can ignore the events and populate it completely from the MatrixRoom
        if (chatEvents.leave) {
          collections.chats[roomId] = { leave: true }
          continue
        }

        const chat = this._getChatCreateAction(matrixRoom)
        if (chat) {
          actions.push(chat)
          collections.chats[roomId] = chat
        }
        // Handle memberships and receipts right here since we have to fetch everything
        const matrixRoomState = matrixRoom.getLiveTimeline().getState(EventTimeline.FORWARDS)
        const matrixMembers = matrixRoomState.getMembers()
        for (const matrixMember of matrixMembers) {
          // Used later in users to update the member if needed
          if (!collections.users[matrixMember.userId] ||
              collections.users[matrixMember.userId].action === 'create') {
            collections.users[matrixMember.userId] = { action: 'update', matrixUser: matrixMember.user }
          }

          // Create membership
          try {
            await this._memberships.find(`${roomId}:${matrixMember.userId}`)
          } catch (e) {
            const membership = this._getMembershipCreateAction(matrixMember)
            if (membership) {
              actions.push(membership)
              if (!collections.memberships[roomId]) collections.memberships[roomId] = {}
              collections.memberships[roomId][matrixMember.userId] = membership
            }
          }

          // Create read receipts
          try {
            await this._receipts.find(`${matrixRoom.roomId}:${matrixMember.userId}`)
          } catch (e) {
            const receipt = this._getReceiptCreateAction(matrixRoom, matrixMember.userId)
            if (receipt) {
              actions.push(receipt)
              if (!collections.receipts[roomId]) collections.receipts[roomId] = {}
              collections.receipts[roomId][matrixMember.userId] = receipt
            }
          }
        }
      }
    }

    // Update memberships
    for (const [roomId, members] of Object.entries(this._syncList.memberships)) {
      // If the room is already in collections, we fetched its memberships in room creation
      // If we are leaving it, no need to handle memberships
      if (collections.memberships[roomId] ||
        (collections.chats[roomId] && collections.chats[roomId].leave)) continue
      collections.memberships[roomId] = {}

      for (const matrixMember of Object.values(members)) {
        try {
          // There might be a name or avatar update
          if (!collections.users[matrixMember.userId] ||
              collections.users[matrixMember.userId].action === 'create') {
            collections.users[matrixMember.userId] = { action: 'update', matrixUser: matrixMember.user }
          }
          // Memberships id are built as roomId:userId
          const membership = await this._memberships.find(`${roomId}:${matrixMember.userId}`)
          // Update membership
          const updatedMembership = this._getMembershipUpdateAction(membership, matrixMember)
          if (updatedMembership) {
            actions.push(updatedMembership)
            collections.memberships[roomId][matrixMember.userId] = updatedMembership
          }
        } catch (e) {
          // Create membership
          const membership = this._getMembershipCreateAction(matrixMember)
          if (membership) {
            actions.push(membership)
            collections.memberships[roomId][matrixMember.userId] = membership
          }
        }
      }
    }

    // Receipts
    for (const { matrixRoom, users } of Object.values(this._syncList.receipts)) {
      // If the room is already in receipts collection, we fetched its receipts in room creation
      // If we are leaving it, no need to handle receipts
      if (collections.receipts[matrixRoom.roomId] ||
        (collections.chats[matrixRoom.roomId] && collections.chats[matrixRoom.roomId].leave)) continue
      collections.receipts[matrixRoom.roomId] = {}

      for (const userId of users) {
        try {
          // Receipts id are built as roomId:userId
          const receipt = await this._receipts.find(`${matrixRoom.roomId}:${userId}`)
          // Update receipt
          const updatedReceipt = this._getReceiptUpdateAction(receipt, matrixRoom, userId)
          if (updatedReceipt) {
            actions.push(updatedReceipt)
            collections.receipts[matrixRoom.roomId][userId] = updatedReceipt
          }
        } catch (e) {
          // It doesn't exist, create new receipt
          const receipt = this._getReceiptCreateAction(matrixRoom, userId)
          if (receipt) {
            actions.push(receipt)
            collections.receipts[matrixRoom.roomId][userId] = receipt
          }
        }
      }
    }
    return actions
  }

  _getChatCreateAction (matrixRoom) {
    const matrixRoomState = matrixRoom.getLiveTimeline().getState(EventTimeline.FORWARDS)
    try {
      const newChat = this._chats.prepareCreate(chat => {
        chat._raw.id = matrixRoom.roomId
        chat.name = matrixRoom.name
        chat.description = matrix.getRoomDescription(matrixRoomState)
        chat.status = matrixRoom.getMember(matrix.getUserId()).membership
        chat.direct = matrix.isRoomDirect(matrixRoom.roomId)
        chat.avatar = matrix.getRoomAvatar(matrixRoom)
        chat.atStart = !matrixRoom.getLiveTimeline().getPaginationToken(EventTimeline.BACKWARDS)
        chat.fullyRead = matrix.getRoomFullyRead(matrixRoom)
        chat.tags = { ...matrixRoom.tags }
        chat.aliases = [...matrixRoom.getAliases()]
        chat.settings = this._getUpdatedSettings(null, matrixRoomState)
      })
      return newChat
    } catch (e) {
      debug('Error creating chat %s:', matrixRoom.roomId, e)
      return null
    }
  }

  async _getChatRemoveActions (chat) {
    const actions = []

    try {
      actions.push(chat.prepareDestroyPermanently())

      const memberships = await this._memberships.query(Q.where('room_id', chat.id)).fetch()
      for (const membership of memberships) {
        actions.push(membership.prepareDestroyPermanently())
      }

      const receipts = await this._receipts.query(Q.where('room_id', chat.id)).fetch()
      for (const receipt of receipts) {
        actions.push(receipt.prepareDestroyPermanently())
      }

      return actions
    } catch (e) {
      debug('Error getting empty chat actions:', e)
      return actions
    }
  }

  _getChatUpdateAction (chat, matrixRoom, events) {
    try {
      const matrixRoomState = matrixRoom.getLiveTimeline().getState(EventTimeline.FORWARDS)
      const chatChanges = {}
      for (const eventType of events) {
        switch (eventType) {
          case 'm.room.create':
            // Chat has already been created
            break
          case 'm.room.name':
          case 'm.room.member': {
            // In a DM, the room name is usually the other member's display name,
            // so we might need to update it
            if (matrixRoom.name !== chat.name) {
              chatChanges.name = matrixRoom.name
            }
            // In a DM, the room avatar is usually the other member's avatar, so
            // we might need to update it
            const avatar = matrix.getRoomAvatar(matrixRoom)
            if (chat.avatar !== avatar) {
              chatChanges.avatar = avatar
            }
            // If we were invited and we joined, change the status
            const newStatus = matrixRoom.getMember(matrix.getUserId()).membership
            if (newStatus !== chat.status) {
              chatChanges.status = newStatus
            }
            break
          }
          case 'm.room.avatar': {
            // We might have already changed the avatar with m.room.member
            if (!chatChanges.avatar) {
              const avatar = matrix.getRoomAvatar(matrixRoom)
              if (avatar !== chat.avatar) {
                chatChanges.avatar = avatar
              }
            }
            break
          }
          case 'm.room.topic': {
            const description = matrix.getRoomDescription(matrixRoomState)
            if (description !== chat.description) {
              chatChanges.description = description
            }
            break
          }
          case 'm.fully_read': {
            const fullyRead = matrix.getRoomFullyRead(matrixRoom)
            if (fullyRead !== chat.fullyRead) {
              chatChanges.fullyRead = fullyRead
            }
            break
          }
          case 'settings': {
            const updatedSettings = this._getUpdatedSettings(chat, matrixRoomState)
            if (updatedSettings) {
              chatChanges.settings = updatedSettings
            }
            break
          }
          case 'start':
            if (!chat.atStart) {
              chatChanges.atStart = true
            }
            break
          default:
            debug('Unhandled update event type for chat %s', matrixRoom.roomId, eventType)
        }
      }

      // Check if DM state changed
      const matrixDirect = matrix.isRoomDirect(matrixRoom.roomId)
      if (chat.direct !== matrixDirect) {
        chatChanges.direct = matrixDirect
      }

      if (Object.keys(chatChanges).length > 0) {
        try {
          const updatedChat = chat.prepareUpdate(chat => {
            for (const [key, value] of Object.entries(chatChanges)) {
              chat[key] = value
            }
          })
          return updatedChat
        } catch (e) {
          debug('Error updating chat %s:', chat.id, e)
          return null
        }
      }
    } catch (e) {
      debug('Error fetching update data for chat %s:', chat.id, e)
    }
    return null
  }

  _getMembershipCreateAction (matrixMember) {
    try {
      const newMembership = this._memberships.prepareCreate(membership => {
        // Build ids as roomId:userId to make sure they're unique per room and per user
        membership._raw.id = `${matrixMember.roomId}:${matrixMember.userId}`
        membership.room.id = matrixMember.roomId
        membership.user.id = matrixMember.userId
        membership.state = matrixMember.membership
        membership.powerLevel = matrixMember.powerLevel
      })
      return newMembership
    } catch (e) {
      debug('Error creating membership for chat %s and user %s:', matrixMember.roomId, matrixMember.userId, e)
      return null
    }
  }

  _getMembershipUpdateAction (membership, matrixMember) {
    const membershipChanges = []
    if (membership.state !== matrixMember.membership) {
      membershipChanges.push({
        prop: 'state',
        value: matrixMember.membership
      })
    }
    if (membership.powerLevel !== matrixMember.powerLevel) {
      membershipChanges.push({
        prop: 'powerLevel',
        value: matrixMember.powerLevel
      })
    }
    if (membershipChanges.length > 0) {
      try {
        const updatedMembership = membership.prepareUpdate(membership => {
          for (const change of membershipChanges) {
            membership[change.prop] = change.value
          }
        })
        return updatedMembership
      } catch (e) {
        debug('Error updating membership for chat %s and user %s:', membership.room.id, matrixMember.userId, e)
        return null
      }
    } else {
      return null
    }
  }

  _getReceiptCreateAction (matrixRoom, userId) {
    try {
      const matrixReceipt = matrix.getUserReadReceiptInRoom(matrixRoom, userId)
      if (matrixReceipt) {
        const newReceipt = this._receipts.prepareCreate(receipt => {
          // Build ids as roomId:userId to make sure they're unique per room and per user
          receipt._raw.id = `${matrixRoom.roomId}:${userId}`
          receipt.room.id = matrixRoom.roomId
          receipt.user.id = userId
          receipt.type = 'read'
          receipt.message.id = matrixReceipt.eventId
          receipt.sent = matrixReceipt.data?.ts
        })
        return newReceipt
      } else {
        // No receipt, happens with room invites
        return null
      }
    } catch (e) {
      debug('Error creating read receipt for chat %s and user %s:', matrixRoom.roomId, userId, e)
      return null
    }
  }

  _getReceiptUpdateAction (readReceipt, matrixRoom, userId) {
    const matrixReceipt = matrix.getUserReadReceiptInRoom(matrixRoom, userId)
    if (readReceipt.message.id !== matrixReceipt.eventId) {
      try {
        const updatedReceipt = readReceipt.prepareUpdate(receipt => {
          // If the event changed, the timestamp changed too
          receipt.message.id = matrixReceipt.eventId
          receipt.sent = matrixReceipt.data?.ts
        })
        return updatedReceipt
      } catch (e) {
        debug('Error updating receipt for chat %s and user %s:', readReceipt.room.id, readReceipt.user.id, e)
        return null
      }
    } else {
      return null
    }
  }

  _getUpdatedSettings (chat, matrixRoomState) {
    let settingsChanges = false
    const updatedSettings = {}
    const settings = chat?.settings || {}
    const matrixRoomGuestAccess = matrixRoomState?.getStateEvents('m.room.guest_access', '')?.getContent()
    const guestAccess = matrixRoomGuestAccess ? matrixRoomGuestAccess.guest_access : null
    if (settings.guestAccess !== guestAccess) {
      updatedSettings.guestAccess = guestAccess
      settingsChanges = true
    }
    const matrixRoomHistoryVisibility = matrixRoomState?.getStateEvents('m.room.history_visibility', '')?.getContent()
    const historyVisibility = matrixRoomHistoryVisibility ? matrixRoomHistoryVisibility.history_visibility : null
    if (settings.historyVisibility !== historyVisibility) {
      updatedSettings.historyVisibility = historyVisibility
      settingsChanges = true
    }
    const matrixRoomJoinRules = matrixRoomState?.getStateEvents('m.room.join_rules', '')?.getContent()
    const joinRules = matrixRoomJoinRules ? matrixRoomJoinRules.join_rule : null
    if (settings.joinRules !== joinRules) {
      updatedSettings.joinRules = joinRules
      settingsChanges = true
    }
    const powerLevels = matrixRoomState?.getStateEvents('m.room.power_levels', '')?.getContent()
    if (!_.isEqual(settings.powerLevels, powerLevels)) {
      updatedSettings.powerLevels = powerLevels
      settingsChanges = true
    }
    if (settingsChanges) return updatedSettings
    else return null
  }

  _handleChatStateEvent (matrixEvent, matrixRoomState) {
    const eventType = matrixEvent.getType()
    const syncList = this._isSyncing ? this._nextSyncList : this._syncList
    const chat = syncList.chats[matrixRoomState.roomId] || { events: new Set() }
    if (chat.leave) return

    switch (eventType) {
      case 'm.room.avatar':
      case 'm.room.create':
      case 'm.room.name':
      case 'm.room.topic':
        // Only store the event type since we're gonna fetch the latest data from the client
        if (!chat.events.has(eventType)) chat.events.add(eventType)
        break
      case 'm.room.encryption':
      case 'm.room.guest_access':
      case 'm.room.history_visibility':
      case 'm.room.join_rules':
      case 'm.room.power_levels':
        // We will fetch all room settings if an update is required,
        // since it's a single object in the database
        if (!chat.events.has('settings')) chat.events.add('settings')
        break
      case 'm.room.member': {
        if (matrixEvent.getStateKey() === matrix.getUserId() &&
            (matrixEvent.getContent().membership === 'leave' ||
            matrixEvent.getContent().membership === 'forget')) {
          debug('We need to leave room %s', matrixRoomState.roomId)
          chat.leave = true
        } else {
          if (!chat.events.has(eventType)) chat.events.add(eventType)
        }
        break
      }
      case 'm.direct':
        syncList.direct = true
        break
      default:
        debug('Unhandled chat event of type %s received:', matrixEvent.getType(), matrixEvent)
    }
    syncList.chats[matrixRoomState.roomId] = chat
    this._isSyncing ? this._nextSyncList = syncList : this._syncList = syncList
  }

  _handleChatAccountEvent (matrixEvent, matrixRoom) {
    const eventType = matrixEvent.getType()
    const syncList = this._isSyncing ? this._nextSyncList : this._syncList
    const chat = syncList.chats[matrixRoom.roomId] || { matrixRoom, events: new Set() }

    switch (eventType) {
      case 'm.fully_read':
        // Only store the event type since we're gonna fetch the latest data from the client
        if (!chat.events.has(eventType)) chat.events.add(eventType)
        break
      default:
        debug('Unhandled chat event of type %s received:', matrixEvent.getType(), matrixEvent)
    }
    syncList.chats[matrixRoom.roomId] = chat
    this._isSyncing ? this._nextSyncList = syncList : this._syncList = syncList
  }

  _handleMembershipEvent (matrixMember) {
    const syncList = this._isSyncing ? this._nextSyncList : this._syncList
    const members = syncList.memberships[matrixMember.roomId] || {}
    if (!members[matrixMember.userId]) {
      members[matrixMember.userId] = matrixMember
      syncList.memberships[matrixMember.roomId] = members
      this._isSyncing ? this._nextSyncList = syncList : this._syncList = syncList
    }
  }

  _handleReceiptEvent (matrixEvent, matrixRoom) {
    const syncList = this._isSyncing ? this._nextSyncList : this._syncList
    if (!syncList.receipts[matrixRoom.roomId]) {
      syncList.receipts[matrixRoom.roomId] = {
        matrixRoom,
        users: new Set()
      }
    }
    // Just store the userId since we only fetch the latest receipts for this room
    const users = syncList.receipts[matrixRoom.roomId].users
    for (const receipt of Object.values(matrixEvent.getContent())) {
      for (const userId in receipt['m.read']) {
        if (!users.has(userId)) {
          users.add(userId)
        }
      }
    }
    syncList.receipts[matrixRoom.roomId].users = users
    this._isSyncing ? this._nextSyncList = syncList : this._syncList = syncList
  }

  _handleStartReached (matrixRoom) {
    const syncList = this._isSyncing ? this._nextSyncList : this._syncList
    const chat = syncList.chats[matrixRoom.roomId] || { matrixRoom, events: new Set() }
    if (!chat.events.has('start')) chat.events.add('start')
    syncList.chats[matrixRoom.roomId] = chat
    this._isSyncing ? this._nextSyncList = syncList : this._syncList = syncList
  }

  _handleTypingEvent (matrixEvent, matrixMember) {
    const users = matrixEvent.getContent().user_ids
    if (!this._ephemeral[matrixMember.roomId]) this._ephemeral[matrixMember.roomId] = {}
    if (!this._ephemeral[matrixMember.roomId].typing) {
      this._ephemeral[matrixMember.roomId].typing = new BehaviorSubject(users)
    } else {
      this._ephemeral[matrixMember.roomId].typing.next(users)
    }
  }

  _isTyping (roomId, isTyping, timeout) {
    if (!this._local[roomId]) this._local[roomId] = {}
    if (!this._local[roomId].typing) this._local[roomId].typing = {}
    const typing = this._local[roomId].typing
    if (!typing.timer && isTyping && !typing.isTyping) {
      typing.timer = setTimeout(() => {
        typing.timer = null
        typing.isTyping = false
      }, TYPING_TIMEOUT)
      typing.isTyping = true
      matrix.sendTyping(roomId, true, TYPING_TIMEOUT + 5000)
    } else if (!isTyping && typing.isTyping) {
      if (typing.timer) {
        clearTimeout(typing.timer)
        typing.timer = null
      }
      typing.isTyping = false
      matrix.sendTyping(roomId, false)
    }
  }
}

const chatService = new ChatService()
export default chatService
