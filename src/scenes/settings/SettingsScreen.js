import withObservables from '@nozbe/with-observables'
import { useNavigation } from '@react-navigation/native'
import {
  Avatar,
  Button,
  Icon,
  Input,
  Layout,
  ListItem,
  Text,
  Toggle,
  TopNavigation,
  useTheme
} from '@ui-kitten/components'
import { useObservableState } from 'observable-hooks'
import React, { useEffect, useState } from 'react'
import {
  ActivityIndicator,
  Linking,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View
} from 'react-native'
import { getApplicationName, getVersion } from 'react-native-device-info'
import ImagePicker from 'react-native-image-picker'
import Touchable from 'react-native-platform-touchable'

import ExitIcon from '../../assets/icons/icon-exit.svg'
import notifications from '../../services/notifications/notifications'
import { isIos } from '../../utilities/Misc'
import auth from '../auth/auth'
import users from '../user/users'

const debug = require('debug')('ditto:scenes:settings:SettingsScreen')

const avatarSize = 60

function SettingsScreen ({ user }) {
  //* *******************************************************************************
  // Properties
  //* *******************************************************************************
  const pushNotifications = useObservableState(notifications.getState())

  const [name, setName] = useState(user.name)
  const [notifsSwitch, setNotifsSwitch] = useState({
    value: pushNotifications.enabled && pushNotifications.pushkey !== null,
    waiting: false
  })

  const navigation = useNavigation()
  const theme = useTheme()

  //* *******************************************************************************
  // Methods
  //* *******************************************************************************
  const chooseAvatar = () => {
    debug('chooseAvatar')
    const options = { title: 'Choose avatar', mediaType: 'photo' }
    try {
      ImagePicker.showImagePicker(options, async response => {
        users.setAvatar(user, response)
      })
    } catch (e) {
      debug('chooseAvatar error', e)
    }
  }

  const saveName = () => {
    if (user.name !== name) {
      users.setName(user.id, name)
    }
  }

  const switchNotifications = enable => {
    if (isIos()) return
    if (enable) {
      setNotifsSwitch({
        value: true,
        waiting: true
      })
      debug('Switching on notifications')
      notifications.enable()
    } else {
      setNotifsSwitch({
        value: false,
        waiting: true
      })
      debug('Switching off notifications')
      notifications.disable()
    }
  }

  const renderRightControls = () => (
    <Touchable onPress={() => navigation.goBack(null)} style={{ padding: 15 }}>
      <ExitIcon fill={theme['text-basic-color']} style={{ width: 40, height: 40 }} />
    </Touchable>
  )

  const logout = async () => {
    auth.logout()
  }

  //* *******************************************************************************
  // Lifecycle
  //* *******************************************************************************

  useEffect(() => {
    setNotifsSwitch({
      value: pushNotifications.enabled && pushNotifications.pushkey != null,
      waiting: false
    })
  }, [pushNotifications])

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: theme['background-basic-color-2'] }}>
      <TopNavigation
        style={{ backgroundColor: theme['background-basic-color-2'] }}
        title='Settings'
        alignment='center'
        rightControls={renderRightControls()}
        titleStyle={{ fontSize: 18 }}
      />
      <Layout level='4' style={{ flex: 1 }}>
        <ScrollView contentContainerStyle={{ marginTop: 24 }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <Touchable style={{ width: avatarSize, height: avatarSize, marginRight: 12 }} onPress={chooseAvatar}>
              <Avatar style={{ width: avatarSize, height: avatarSize }} source={{ uri: user.avatar ? users.getAvatarUrl(user.avatar) : null }} />
            </Touchable>
            <Input
              size={isIos() ? 'large' : 'small'}
              onChangeText={setName}
              placeholder='Set display name'
              value={name}
              onBlur={saveName}
              style={{ minWidth: 150 }}
              label='Display Name'
            />
          </View>
          <ListItem
            style={{ backgroundColor: theme['background-basic-color-3'], marginTop: 30 }}
            title='Enable Notifications'
            description='Android only (for now!)'
            accessory={() => notifsSwitch.waiting ? (
              <ActivityIndicator animating={notifsSwitch.waiting} />
            ) : (
              <Toggle
                onChange={switchNotifications}
                checked={notifsSwitch.value}
                disabled={notifsSwitch.waiting}
              />
            )}
          />
          <ListItem
            style={{ backgroundColor: theme['background-basic-color-3'], marginTop: 1 }}
            title={`${getApplicationName()} Version`}
            accessory={() => <Text>{getVersion()}</Text>}
          />

          <Text category='h6' appearance='hint' style={{ marginLeft: 15, marginTop: 40, marginBottom: 10 }}>Links</Text>
          <CustomListItem title='Website' url='https://dittochat.org' iconName='globe-2' iconColor={theme['color-info-500']} />
          <CustomListItem title='Privacy Policy' url='https://dittochat.org/privacy' iconName='eye' iconColor={theme['color-success-500']} />
          <CustomListItem title='Request a Feature' url='https://plan.dittochat.org' iconName='gift' iconColor={theme['color-warning-500']} />
          <CustomListItem title='Report a Problem' url='https://gitlab.com/ditto-chat/ditto-mobile/issues' iconName='alert-triangle' iconColor={theme['color-danger-500']} />

          <Button onPress={logout} appearance='outline' status='danger' style={{ marginTop: 60, marginHorizontal: 15, marginBottom: 50 }}>Logout</Button>
        </ScrollView>
      </Layout>

    </SafeAreaView>
  )
}
const enhance = withObservables(['auth', 'users'], () => ({
  user: users.getUser(auth.getUserId())
}))
export default enhance(SettingsScreen)

const CustomListItem = ({ title, iconName, iconColor, url }) => {
  const theme = useTheme()
  const onPress = () => {
    try {
      debug('open website')
      Linking.openURL(url)
    } catch (err) {
      debug('open website error', err)
    }
  }
  return (
    <TouchableOpacity onPress={onPress} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 1, backgroundColor: theme['background-basic-color-3'] }}>
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <Button appearance='ghost' icon={style => <Icon {...style} name={iconName} fill={iconColor} />} />
        <Text category='s2'>{title}</Text>
      </View>
      <Button appearance='ghost' icon={style => <Icon {...style} style={{ marginRight: 0 }} name='chevron-right' fill={theme['text-hint-color']} width={25} height={25} />} />
    </TouchableOpacity>

  )
}
