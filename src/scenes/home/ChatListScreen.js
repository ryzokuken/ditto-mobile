import withObservables from '@nozbe/with-observables'
import { useNavigation } from '@react-navigation/native'
import {
  Layout,
  useTheme
} from '@ui-kitten/components'
import { useObservableState } from 'observable-hooks'
import React, { useEffect, useState } from 'react'
import FAB from 'react-native-fab'
import Icon from 'react-native-vector-icons/Feather'
import styled from 'styled-components/native'

import storage from '../../services/storage/service'
import { isIos } from '../../utilities/Misc'
import chats from '../chat/chats'
import messages from '../chat/message/messages'
import ChatList from './components/ChatList'

// const debug = require('debug')('ditto:scenes:home:components:ChatList')

function ChatListScreen ({ chatList, messageCount, direct }) {
  const [sortedChats, setSortedChats] = useState([])
  const hasFirstSync = useObservableState(storage.hasFirstSync())

  const theme = useTheme()
  const navigation = useNavigation()

  const openNewChat = () => {
    navigation.navigate('NewChat')
  }

  useEffect(() => {
    const sortedChats = [...chatList]
    sortedChats.sort((a, b) => {
      const messageA = messages.getLatestMessage(a.id)?.getValue()
      const messageB = messages.getLatestMessage(b.id)?.getValue()

      if (messageA && messageB) {
        return messageA.sent < messageB.sent ? 1 : messageA.sent > messageB.sent ? -1 : 0
      } else if (messageA) {
        return -1
      } else if (messageB) {
        return 1
      } else return 0
    })
    setSortedChats(sortedChats)
  }, [chatList, messageCount])

  return (
    <Layout level='4' style={{ height: '100%' }}>
      {!hasFirstSync && (
        <SyncingIndicator>
          <SyncingText>Syncing...</SyncingText>
        </SyncingIndicator>
      )}
      <ChatList
        chatList={sortedChats}
      />

      {!isIos() && (
        // eslint-disable-next-line react/jsx-pascal-case
        <FAB
          buttonColor={theme['color-primary-active']}
          iconTextColor={theme['text-basic-color']}
          onClickAction={openNewChat}
          visible
          iconTextComponent={<Icon name='plus' />}
        />
      )}
    </Layout>
  )
}
const enhance = withObservables(['chats', 'direct'], ({ direct = false }) => ({
  chatList: chats.getChatsByDirect(direct),
  messageCount: messages.getCount()
}))
export default enhance(ChatListScreen)

const SyncingIndicator = styled.View`
  background-color: #4e22be;
  height: 20;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`

const SyncingText = styled.Text`
  color: #fff;
  font-style: italic;
  font-size: 12;
`
