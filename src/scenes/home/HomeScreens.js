import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import { useTheme } from '@ui-kitten/components'
import React from 'react'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import GroupsIcon from '../../assets/icons/icon-groups.svg'
import { isIphoneX } from '../../utilities/isIphoneX'
import ChatListScreen from './ChatListScreen'

const Tab = createMaterialTopTabNavigator()

const options = (theme) => {
  return {
    tabBarPosition: 'bottom',
    swipeEnabled: false,
    tabBarOptions: {
      showIcon: true,
      activeTintColor: theme['text-basic-color'],
      inactiveTintColor: theme['text-hint-color'],
      style: {
        backgroundColor: theme['background-basic-color-2'],
        paddingLeft: 40,
        paddingRight: 40,
        height: isIphoneX() ? 75 : 60
      },
      indicatorStyle: {
        height: 0
      },
      tabStyle: {
        alignSelf: 'center',
        padding: 0
      },
      labelStyle: {
        fontWeight: '700',
        fontSize: 10
      },
      iconStyle: {
        marginTop: 5
      }
    }

  }
}
export default function HomeScreens () {
  const DirectChatList = (props) => (<ChatListScreen {...props} direct />)
  const theme = useTheme()

  return (
    <Tab.Navigator {...options(theme)}>
      <Tab.Screen name='Messages' options={{ tabBarIcon: peopleIcon }} component={DirectChatList} />
      <Tab.Screen name='Groups' options={{ tabBarIcon: groupsIcon }} component={ChatListScreen} />
    </Tab.Navigator>
  )
}

const iconSize = 25
const size = {
  width: iconSize,
  height: iconSize
}
const peopleIcon = ({ color }) => <Icon name='checkbox-multiple-blank-circle' size={iconSize} color={color} />
const groupsIcon = ({ color }) => <GroupsIcon {...size} fill={color} />
