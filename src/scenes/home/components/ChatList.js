import { useNavigation } from '@react-navigation/native'
import {
  Button,
  Icon as EvaIcon,
  useTheme
} from '@ui-kitten/components'
import React, { useEffect, useState } from 'react'
import { Alert, Animated, FlatList } from 'react-native'
import Swipeable from 'react-native-gesture-handler/Swipeable'
import styled from 'styled-components/native'

import chats from '../../chat/chats'
import ChatListItem from './ChatListItem'

const debug = require('debug')('ditto:scenes:home:components:ChatList')

export default function ChatList ({ chatList }) {
  const [swipableRefs, setSwipableRefs] = useState([])

  const theme = useTheme()
  const navigation = useNavigation()

  const navigateToChat = chat => {
    recenterSwipeables()
    navigation.navigate('Chat', {
      roomId: chat.id,
      roomTitle: chat.name
    })
  }

  const openLeftEditButtons = (index) => {
    recenterSwipeables()
    swipableRefs[index].openLeft()
  }

  const recenterSwipeables = () => {
    swipableRefs.forEach(ref => ref && ref.close())
  }

  const renderLeftActions = (progress, dragX, chat) => {
    const trans = dragX.interpolate({
      inputRange: [0, 40, 60, 80],
      outputRange: [-50, 0, 0, 0]
    })
    return (
      <Animated.View
        style={[{ transform: [{ translateX: trans }] }, { height: '100%', justifyContent: 'center', marginLeft: 12 }]}
      >
        <ActionButton
          onPress={() => leaveChat(chat)}
          appearance='filled'
          style={{ borderWidth: 0, backgroundColor: theme['background-basic-color-1'] }}
          icon={style => <EvaIcon {...style} name='trash' width={20} height={20} />}
        />
      </Animated.View>
    )
  }

  const leaveChat = async (chat) => {
    Alert.alert(
      'Leave Chat',
      `Are you sure you would like to leave "${chat.name}"?`,
      [
        {
          text: 'Stay',
          onPress: () => {}
        },
        {
          text: 'Leave',
          onPress: () => confirmLeaveChat(chat.id),
          style: 'destructive'
        }
      ],
      { cancelable: false }
    )
  }

  const confirmLeaveChat = async (id) => {
    try {
      chats.leaveChat(id)
    } catch (e) {
      debug('leaveRoom error', e)
    }
  }

  useEffect(() => {
    setSwipableRefs(Array(chatList.length).fill(null))
  }, [chatList])

  return (
    <FlatList
      onScroll={recenterSwipeables}
      style={{ backgroundColor: 'transparent' }}
      data={chatList}
      renderItem={({ item: chat, index }) => (
        <Swipeable enabled={false} ref={ref => { swipableRefs[index] = ref }} renderLeftActions={(p, d) => renderLeftActions(p, d, chat)} leftThreshold={100} overshootFriction={8}>
          <ChatListItem
            onLongPress={() => openLeftEditButtons(index)}
            onPress={() => navigateToChat(chat)}
            chat={chat}
          />
        </Swipeable>
      )}
    />
  )
}

const ActionButton = styled(Button)`
  width: 25;
  height: 25;
  margin-right: 3;
  border-radius: 50;
  align-self: flex-end;
  justify-content: center;
  align-items: center;
`
