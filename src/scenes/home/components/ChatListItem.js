import withObservables from '@nozbe/with-observables'
import { Avatar, Text, useTheme } from '@ui-kitten/components'
import Color from 'color'
import moment from 'moment'
import { useObservableState } from 'observable-hooks'
import React, { useEffect, useState } from 'react'
import { View } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import styled from 'styled-components/native'

import { getNameColor } from '../../../utilities/Misc'
import chats from '../../chat/chats'
import messages from '../../chat/message/messages'
import ChatTitle from './ChatTitle'

// const debug = require('debug')('ditto:scenes:home:components:ChatListItem')

const avatarSize = 56
const Placeholder = require('../../../assets/images/placeholder.png')

function ChatListItem ({ onLongPress, onPress, chat, joinedMembersCount, receipts }) {
  const theme = useTheme()
  const [unread, setUnread] = useState(false)
  const [readByAll, setReadByAll] = useState(false)
  const latestMessage = useObservableState(messages.getLatestMessage(chat.id))
  const typing = useObservableState(chats.getTyping(chat.id))

  const renderReadIndicator = () => {
    if (unread) {
      return <View style={{ width: 16, height: 16, backgroundColor: theme['color-primary-active'], borderRadius: 50 }} />
    }
    if (readByAll) {
      return <Icon name='md-mail-open' color={theme['color-basic-800']} size={18} />
    } else {
      return <Icon name='md-mail' color={theme['color-basic-800']} size={18} />
    }
  }

  useEffect(() => {
    const myReadReceipt = chats.getMyReadReceipt(receipts)
    if (latestMessage) {
      if (latestMessage.id !== myReadReceipt?.message.id) setUnread(true)
      else setUnread(false)

      if (receipts.length === joinedMembersCount &&
          receipts.every(receipt => receipt.message.id === latestMessage.id)) {
        setReadByAll(true)
      } else setReadByAll(false)
    } else {
      setUnread(false)
      setReadByAll(false)
    }
  }, [latestMessage, joinedMembersCount, receipts])

  return (
    <Item onLongPress={onLongPress} onPress={onPress} style={unread ? { backgroundColor: theme['background-basic-color-3'] } : {}}>
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        {chat.avatar ? (
          <Avatar size='giant' source={{ uri: chats.getAvatarUrl(chat.avatar, avatarSize) }} defaultSource={Placeholder} />
        ) : (
          <View
            style={{
              position: 'relative',
              width: avatarSize,
              height: avatarSize
            }}
            shouldRasterizeIOS
          >
            <AvatarTextWrapper>
              <Text category='h5'>{chat.name[0].toUpperCase()}</Text>
            </AvatarTextWrapper>
            <AvatarBackground roomName={chat.name} />
          </View>
        )}

        <ChatTitle chat={chat} unread={unread} latestMessage={latestMessage} typing={typing} />
      </View>

      <ItemAccessory>
        <Text
          category='c1'
          appearance='hint'
          numberOfLines={1}
          style={{ marginBottom: 4 }}
        >
          {latestMessage ? moment(latestMessage?.sent).fromNow() : null}
        </Text>
        {renderReadIndicator()}
      </ItemAccessory>
    </Item>
  )
}
const enhance = withObservables(['chat'], ({ chat }) => ({
  joinedMembersCount: chats.getJoinedMembersCount(chat.id),
  receipts: chat.receipts.observeWithColumns(['message_id'])
}))
export default enhance(ChatListItem)

const Item = styled.TouchableOpacity`
  padding-left: 14;
  padding-right: 14;
  padding-top: 8;
  padding-bottom: 8;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-top: 0.4;
  margin-bottom: 0.4;
`

const AvatarTextWrapper = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  height: ${avatarSize};
  width: ${avatarSize};
  justify-content: center;
  align-items: center;
  z-index: 2;
`

const AvatarBackground = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  height: ${avatarSize};
  width: ${avatarSize};
  border-radius: 40;
  background-color: ${({ roomName }) =>
    Color(getNameColor(roomName))
      .darken(0.5)
      .hex()};
  z-index: 1;
`

const ItemAccessory = styled.View`
align-items: flex-end
`
