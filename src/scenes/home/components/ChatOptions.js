import React from 'react'
import styled from 'styled-components/native'

import ExitIcon from '../../../assets/icons/icon-exit.svg'
import Text from '../../../components/Text'
import { COLORS } from '../../../constants'

const ChatOptions = ({ visible, onRequestClose, room, leaveRoom }) => {
  return (
    <OptionsModal
      animationType='slide'
      visible={visible}
      onRequestClose={onRequestClose}
      transparent
    >
      <ModalWrapper>
        <OptionsHeader>
          <Text>Options</Text>
          <ExitWrapper onPress={onRequestClose}>
            <ExitIcon width={15} height={15} fill={COLORS.gray.one} />
          </ExitWrapper>
        </OptionsHeader>
        <RoomName>{room.name}</RoomName>
        <Spacer />
        <Spacer />
        <LeaveLinkWrapper onPress={leaveRoom}>
          <LeaveLink>Leave Conversation</LeaveLink>
        </LeaveLinkWrapper>
      </ModalWrapper>
    </OptionsModal>
  )
}

const OptionsModal = styled.Modal``

const ModalWrapper = styled.View`
  position: relative;
  top: 25%;
  background-color: ${COLORS.headerColor};
  margin-right: 16;
  margin-left: 16;
  border-radius: 8;
  padding-top: 16;
  padding-bottom: 16;
  padding-right: 16;
  padding-left: 16;
`

const OptionsHeader = styled.View`
  flex-direction: row;
  justify-content: center;
  font-weight: 600;
`

const LeaveLink = styled.Text`
  color: ${COLORS.red};
  text-align: center;
  font-size: 20;
`

const Spacer = styled.View`
  background-color: ${COLORS.headerColor};
  height: 40;
`

const ExitWrapper = styled.TouchableOpacity`
  position: absolute;
  right: 4;
`

const RoomName = styled(Text)`
  text-align: center;
  font-weight: 400;
`

const LeaveLinkWrapper = styled.TouchableOpacity``

export default ChatOptions
