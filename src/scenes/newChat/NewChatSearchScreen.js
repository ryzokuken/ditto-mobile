import { useNavigation } from '@react-navigation/native'
import { Layout, TopNavigation, useTheme } from '@ui-kitten/components'
import React, { useState } from 'react'
import { SafeAreaView, View } from 'react-native'
import Touchable from 'react-native-platform-touchable'
import styled from 'styled-components/native'

import ChevronsUp from '../../assets/icons/icon-chevrons-up.svg'
import ExitIcon from '../../assets/icons/icon-exit.svg'
import ChipInput from './components/ChipInput'

const debug = require('debug')('ditto:scenes:newChat:NewChatSearchScreen')

export default function NewChatSearchScreen () {
  //* *******************************************************************************
  // Properties
  //* *******************************************************************************
  const [selectedUsers, setSelectedUsers] = useState([])

  const navigation = useNavigation()
  const theme = useTheme()

  //* *******************************************************************************
  // Methods
  //* *******************************************************************************
  const updateSelectedUsers = selectedUsers => setSelectedUsers(selectedUsers)

  const handleConfirmMembers = () => {
    debug('selected members', selectedUsers)
    navigation.navigate('NewChatDetails', {
      usersToInvite: selectedUsers
    })
  }

  const renderRightControls = () => (
    <Touchable onPress={() => navigation.goBack(null)} style={{ padding: 15 }}>
      <ExitIcon fill={theme['text-basic-color']} style={{ width: 40, height: 40 }} />
    </Touchable>
  )

  //* *******************************************************************************
  // Lifecycle
  //* *******************************************************************************
  return (
    <SafeAreaView style={{ backgroundColor: theme['background-basic-color-2'] }}>
      <TopNavigation title='New Chat' alignment='center' rightControls={renderRightControls()} style={{ backgroundColor: theme['background-basic-color-2'] }} titleStyle={{ fontSize: 18 }} />
      <Layout level='4' style={{ height: '100%' }}>
        <View style={{ marginTop: 12, flexDirection: 'row', justifyContent: 'center' }}>
          <ChipInput
            updateSelectedUsers={updateSelectedUsers}
          />
          <ActionButton
            style={{ padding: 10, backgroundColor: theme[selectedUsers.length === 0 ? 'color-primary-transparent-500' : 'color-primary-active'] }}
            onPress={handleConfirmMembers}
            disabled={selectedUsers.length === 0}
          >
            <ChevronsUp
              fill={theme['text-basic-color']}
              width={16}
              height={16}
              style={{ transform: [{ rotate: '90deg' }], marginLeft: 1, opacity: selectedUsers.length === 0 ? 0.5 : 1 }}
            />
          </ActionButton>
        </View>
      </Layout>
    </SafeAreaView>
  )
}

const ActionButton = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  border-radius: 40;
  margin-left: 8;
  margin-right: 8;
  width: 35;
  height: 35;
`
