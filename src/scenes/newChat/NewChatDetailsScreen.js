import { Button, Icon, Input, Layout, ListItem, Toggle, TopNavigation, useTheme } from '@ui-kitten/components'
import React, { useState } from 'react'
import { SafeAreaView } from 'react-native'
import Touchable from 'react-native-platform-touchable'

import ExitIcon from '../../assets/icons/icon-exit.svg'
import { isIos } from '../../utilities/Misc'
import chats from '../chat/chats'

const debug = require('debug')('ditto:scenes:newChat:NewChatDetailsScreen')

export default function NewChatDetailsScreen ({ navigation, route }) {
  const [isPrivate, setIsPrivate] = useState(true)
  const [roomName, setRoomName] = useState('')

  const theme = useTheme()

  const handleCreateChat = async () => {
    const { usersToInvite } = route.params

    const options = {
      visibility: isPrivate ? 'private' : 'public',
      invite: usersToInvite,
      name: roomName,
      room_topic: ''
    }
    debug('Room options: ', options)
    const chat = await chats.createChat(options)
    if (chat) {
      chats.isChatCreated(chat.id).subscribe(created => {
        if (created) {
          navigation.navigate('Chat', { roomId: chat.id, roomTitle: chat.name })
        }
      })
    }
  }

  const renderBackButton = () => (
    <Button
      style={{ backgroundColor: 'transparent', borderColor: 'transparent', paddingLeft: 0 }}
      onPress={() => navigation.goBack()}
      icon={style => <Icon {...style} name={`arrow-${isIos() ? 'ios-' : ''}back`} width={35} height={35} />}
    />
  )

  const renderRightControls = () => (
    <Touchable onPress={() => navigation.navigate('Home')} style={{ padding: 15 }}>
      <ExitIcon fill={theme['text-basic-color']} style={{ width: 40, height: 40 }} />
    </Touchable>
  )

  return (
    <SafeAreaView style={{ backgroundColor: theme['background-basic-color-2'] }}>
      <TopNavigation title='Chat Details' alignment='center' leftControl={renderBackButton()} rightControls={renderRightControls()} style={{ backgroundColor: theme['background-basic-color-2'] }} titleStyle={{ fontSize: 18 }} />
      <Layout level='4' style={{ height: '100%', paddingTop: 24 }}>

        <Input
          label='Chat Name (Optional)'
          value={roomName}
          onChangeText={setRoomName}
          placeholder='Ex. My Awesome Room'
          style={{ marginHorizontal: 12, marginBottom: 18 }}
        />

        <ListItem
          title='Private'
          description='A private chat is only open to those you invite.'
          style={{ backgroundColor: theme['background-basic-color-2'] }}
          accessory={() => (
            <Toggle
              onChange={setIsPrivate}
              checked={isPrivate}
            />
          )}
        />

        <Button onPress={handleCreateChat} style={{ marginHorizontal: 12, marginTop: 24 }}>
          Create Chat
        </Button>
      </Layout>
    </SafeAreaView>
  )
}
