import { BehaviorSubject, Subject } from 'rxjs'
import { map } from 'rxjs/operators'

import database from '../../services/storage/database'

const debug = require('debug')('ditto:scenes:auth:service')

const initialAuthData = { userId: null, accessToken: null, homeserver: null, deviceId: null }

class AuthService {
  // ********************************************************************************
  // Init
  // ********************************************************************************
  _data
  _loaded
  _error
  _action
  _isSyncing
  actions

  constructor () {
    this._data = new BehaviorSubject(initialAuthData)
    this._loaded = new BehaviorSubject(false)
    this._error = new BehaviorSubject(null)
    this._action = new Subject(null)
    this._isSyncing = false
    this.actions = {
      ERROR: 'error',
      FORCED_LOGOUT: 'forcedLogout',
      LOAD: 'load',
      LOGGED_IN: 'loggedIn',
      LOGIN: 'login',
      LOGIN_RESPONSE: 'loginResponse',
      LOGOUT: 'logout'
    }
  }

  async init () {
    this._isSyncing = true
    const jsonData = await database.getLocal('auth')
    const cleanData = this._sanitizeData(JSON.parse(jsonData))
    this._data.next(cleanData)
    this._loaded.next(true)
    this._isSyncing = false

    this.getAction().subscribe((action) => {
      switch (action.type) {
        case this.actions.ERROR:
          this._setError(action.payload)
          break
        case this.actions.FORCED_LOGOUT:
          // TODO: warn user
          this._logout()
          break
        case this.actions.LOGIN_RESPONSE:
          this._setData(action.payload)
          break
        case this.actions.LOGOUT:
          this._logout()
          break
        default:
      }
    })

    if (cleanData.userId && cleanData.accessToken && cleanData.homeserver) {
      this.setAction({ type: this.actions.LOGGED_IN })
    }
  }

  // ********************************************************************************
  // Get/set
  // ********************************************************************************

  getAction () {
    return this._action
  }

  setAction (action) {
    if (!Object.values(this.actions).includes(action.type)) throw Error('Unknown action type:', action)
    debug('Set action:', action)
    this._action.next({ type: action.type, payload: action.payload })
  }

  getData () {
    return this._data
  }

  getError () {
    return this._error
  }

  isLoaded () {
    return this._loaded
  }

  isLoggedIn () {
    return this._data.pipe(map(data => data.userId && data.accessToken && data.homeserver))
  }

  // ********************************************************************************
  // Private methods
  // ********************************************************************************

  async _logout () {
    await this._setData(initialAuthData)
  }

  _sanitizeData (data) {
    return {
      userId: data?.userId,
      accessToken: data?.accessToken,
      homeserver: data?.homeserver,
      deviceId: data?.deviceId
    }
  }

  async _setData (data) {
    if (this._isSyncing) debug('A database interaction is already running')
    this._isSyncing = true
    const cleanData = this._sanitizeData(data)
    this._data.next(cleanData)
    await database.setLocal('auth', JSON.stringify(cleanData))
    this._isSyncing = false

    if (cleanData.userId && cleanData.accessToken && cleanData.homeserver) {
      this.setAction({ type: this.actions.LOGGED_IN })
    }
  }

  _setError (text) {
    this._error.next(text)
  }
}

const authService = new AuthService()
export default authService
