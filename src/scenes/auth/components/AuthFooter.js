import React from 'react'
import styled from 'styled-components/native'

import { TextButton } from '../../../components'
import { SCREEN_WIDTH } from '../../../constants'

const AuthFooterWrapper = styled.SafeAreaView`
  position: absolute;
  bottom: 25;
  width: ${SCREEN_WIDTH};
  align-items: center;
`

const AuthFooter = ({ mainText = 'Main Text', mainAction = () => {} }) => (
  <AuthFooterWrapper>
    <TextButton size='md' onPress={mainAction}>
      {mainText}
    </TextButton>
    <TextButton>Privacy Policy</TextButton>
  </AuthFooterWrapper>
)
export default AuthFooter
