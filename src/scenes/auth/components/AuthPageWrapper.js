import React from 'react'
import LinearGradient from 'react-native-linear-gradient'
import styled from 'styled-components/native'

const gradientColors = ['#673BAC', '#25114B']

const SafeAreaContent = styled.SafeAreaView`
  height: 100%;
  align-items: center;
`

const AuthPageWrapper = ({ children }: any) => (
  <LinearGradient
    colors={gradientColors}
    useAngle
    angle={2}
    angleCenter={{ x: 0.5, y: 0.5 }}
  >
    <SafeAreaContent>{children}</SafeAreaContent>
  </LinearGradient>
)
export default AuthPageWrapper
