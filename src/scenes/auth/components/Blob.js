import React from 'react'
import Image from 'react-native-scalable-image'

const Blob = props => (
  <Image
    source={require('../../assets/images/blob.png')}
    width={100}
    {...props}
  />
)
export default Blob
