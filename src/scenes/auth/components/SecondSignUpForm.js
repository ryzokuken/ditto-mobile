import React, { Component } from 'react'
import styled from 'styled-components/native'

import { Input } from '../../../components'
import { SCREEN_WIDTH } from '../../constants'

export default class SecondSignUpForm extends Component {
  usernameInput = null;
  newPasswordInput = null;
  confirmPasswordInput = null;

  handleUsernameSubmit = () => {
    this.usernameInput.blur()
    this.newPasswordInput.focus()
  };

  handlePasswordSubmit = () => {
    this.newPasswordInput.blur()
    this.confirmPasswordInput.focus()
  };

  render () {
    return (
      <Wrapper>
        <Input
          ref={ref => (this.usernameInput = ref)}
          marginValue='15px 0'
          placeholder='Username'
          returnKeyType='next'
          onSubmitEditing={this.handleUsernameSubmit}
        />
        <Input
          ref={ref => (this.newPasswordInput = ref)}
          secureTextEntry
          marginValue='0 0 15px'
          placeholder='New Password'
          returnKeyType='next'
          onSubmitEditing={this.handlePasswordSubmit}
        />
        <Input
          ref={ref => (this.confirmPasswordInput = ref)}
          secureTextEntry
          marginValue='0 0 15px'
          placeholder='Confirm Password'
          returnKeyType='done'
        />
      </Wrapper>
    )
  }
}

const Wrapper = styled.View`
  width: ${SCREEN_WIDTH};
  padding: 0 20px;
`
