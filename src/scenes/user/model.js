import { Model, tableSchema } from '@nozbe/watermelondb'
import { field, json } from '@nozbe/watermelondb/decorators'

export const userSchema = tableSchema({
  name: 'users',
  columns: [
    { name: 'name', type: 'string' },
    { name: 'avatar', type: 'string' },
    { name: 'presence', type: 'string' },
    { name: 'devices', type: 'string' }
  ]
})

export class UserModel extends Model {
  static table = 'users'

  @field('name') name
  @field('avatar') avatar
  @json('presence', json => json) presence
  @json('devices', json => json) devices
}
