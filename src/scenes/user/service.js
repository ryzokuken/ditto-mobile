import _ from 'lodash'

import matrix from '../../services/matrix/service'
import database from '../../services/storage/database'

const debug = require('debug')('ditto:scenes:users:service')

class UserService {
  _users
  _syncList
  _nextSyncList
  _isSyncing

  constructor () {
    this._users = database.getCollection('users')
    this._syncList = {}
    this._nextSyncList = {}
    this._isSyncing = false
  }

  init () {
    matrix.getAction().subscribe(action => {
      switch (action.type) {
        case matrix.actions.EVENT_USER:
          this._handleUserEvent(action.payload.event, action.payload.user)
          break
        case matrix.actions.EVENT_ROOM_STATE:
          // When a user's display name changes, it triggers an m.room.member
          // that is sometimes a matrix EVENT_ROOM_STATE action so we need to
          // trigger an update
          this._handleRoomStateEvent(action.payload.event)
          break
        default:
      }
    })
  }

  getUsers () {
    return this._users
  }

  async setAvatar (image) {
    try {
      const url = await matrix.uploadImage(image)
      if (!url) return
      await matrix.setAvatar(url)
    } catch (e) {
      debug('Error setting avatar:', e)
    }
  }

  async setName (name) {
    try {
      await matrix.setDisplayName(name)
    } catch (e) {
      debug('Error setting name:', e)
    }
  }

  //* *******************************************************************************
  // Sync
  //* *******************************************************************************

  finishSync () {
    for (const key in this._syncList) {
      delete this._syncList[key]
    }
    this._syncList = {}
  }

  setSyncing (isSyncing) {
    this._isSyncing = isSyncing
  }

  pushNextSyncList () {
    _.merge(this._syncList, this._nextSyncList)
    this._nextSyncList = {}
  }

  async getEmptyAllActions () {
    const actions = []

    try {
      const users = await this._users.query().fetch()
      for (const user of users) {
        actions.push(user.prepareDestroyPermanently())
      }

      return actions
    } catch (e) {
      debug('Error getting empty user actions:', e)
      return actions
    }
  }

  async getSyncActions (collections) {
    debug('User data to sync:', this._syncList)
    debug('User collection data:', collections.users)
    const actions = []

    // Users in syncList (events we listen to directly)
    for (const userId in this._syncList) {
      const matrixUser = this._syncList[userId] || matrix.getUser(userId)
      try {
        const user = await this._users.find(matrixUser.userId)
        // User exists, update it
        const updatedUser = this._getUserUpdateAction(user, matrixUser)
        if (updatedUser) {
          actions.push(updatedUser)
          collections.users[matrixUser.userId] = updatedUser
        }
      } catch (e) {
        // User doesn't exist, create it
        if (matrixUser) {
          const user = this._getUserCreateAction(matrixUser)
          if (user) {
            actions.push(user)
            collections.users[matrixUser.userId] = user
          }
        } else {
          debug('User was null: ', userId)
        }
      }
    }

    // Users we encountered previously (message sender or room member)
    for (const userId in collections.users) {
      // If we already handled it, the user is a database UserModel,
      // else it's just an object with an action
      if (collections.users[userId].action) {
        const matrixUser = collections.users[userId].matrixUser || matrix.getUser(userId)
        try {
          const user = await this._users.find(userId)
          const action = collections.users[userId].action
          if (action === 'delete') {
            actions.push(user.prepareDestroyPermanently())
            collections.users[userId] = { delete: true }
          } else if (action === 'update') {
            // Only update it if we specifically asked for it (m.room.membership events)
            const updatedUser = this._getUserUpdateAction(user, matrixUser)
            if (updatedUser) {
              actions.push(updatedUser)
              collections.users[userId] = updatedUser
            }
          }
        } catch (e) {
          // If it's being deleted, don't do anything
          if (collections.users[userId].action === 'delete') continue
          // User doesn't exist, create it
          const user = this._getUserCreateAction(matrixUser)
          if (user) {
            actions.push(user)
            collections.users[user.id] = user
          }
        }
      }
    }
    return actions
  }

  _getUserCreateAction (matrixUser) {
    try {
      const newUser = this._users.prepareCreate(user => {
        user._raw.id = matrixUser.userId
        user.name = matrixUser.displayName
        user.avatar = matrixUser.avatarUrl
        user.presence = {
          status: matrixUser.presence,
          lastSeen: matrixUser.lastPresenceTs
        }
      })
      return newUser
    } catch (e) {
      debug('Error creating user ', matrixUser, e)
      return null
    }
  }

  _getUserUpdateAction (user, matrixUser) {
    try {
      const userChanges = []
      if (user.name !== matrixUser.displayName) {
        userChanges.push({
          prop: 'name',
          value: matrixUser.displayName
        })
      }
      if (user.avatar !== matrixUser.avatarUrl) {
        userChanges.push({
          prop: 'avatar',
          value: matrixUser.avatarUrl
        })
      }
      if (user.presence.status !== matrixUser.presence ||
          user.presence.lastSeen !== matrixUser.lastPresenceTs) {
        userChanges.push({
          prop: 'name',
          value: {
            status: matrixUser.presence,
            lastSeen: matrixUser.lastPresenceTs
          }
        })
      }
      if (userChanges.length > 0) {
        const updatedUser = user.prepareUpdate(user => {
          for (const change of userChanges) {
            user[change.prop] = change.value
          }
          user.name = matrixUser.displayName
          user.avatar = matrixUser.avatarUrl
          user.presence = {
            status: matrixUser.presence,
            lastSeen: matrixUser.lastPresenceTs
          }
        })
        return updatedUser
      } else {
        return null
      }
    } catch (e) {
      debug('Error updating user %s:', matrixUser.userId, e)
      return null
    }
  }

  _handleUserEvent (matrixEvent, matrixUser) {
    const syncList = this._isSyncing ? this._nextSyncList : this._syncList
    switch (matrixEvent.getType()) {
      case 'm.presence':
        if (!syncList[matrixUser.userId]) {
          syncList[matrixUser.userId] = matrixUser
        }
        break
      default:
        debug('Unhandled user event of type %s received:', matrixEvent.getType(), matrixEvent)
    }
    this._isSyncing ? this._nextSyncList = syncList : this._syncList = syncList
  }

  _handleRoomStateEvent (matrixEvent) {
    const syncList = this._isSyncing ? this._nextSyncList : this._syncList
    switch (matrixEvent.getType()) {
      case 'm.room.member':
        if (!syncList[matrixEvent.getStateKey()]) {
          syncList[matrixEvent.getStateKey()] = null
        }
        break
      default:
    }
    this._isSyncing ? this._nextSyncList = syncList : this._syncList = syncList
  }
}

const userService = new UserService()
export default userService
