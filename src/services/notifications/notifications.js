import notificationService from './service'

// const debug = require('debug')('ditto:scenes:settings:settings')

class Notifications {
  disable () {
    notificationService.setAction({ type: notificationService.actions.DISABLE })
  }

  enable () {
    notificationService.setAction({ type: notificationService.actions.ENABLE })
  }

  getState () {
    return notificationService.getState()
  }
}

const notifications = new Notifications()
export default notifications
