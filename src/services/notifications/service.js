import { BehaviorSubject, Subject } from 'rxjs'

import auth from '../../scenes/auth/service'
import chats from '../../scenes/chat/service'
import { isIos } from '../../utilities/Misc'
import matrix from '../matrix/service'
import database from '../storage/database'
import NotificationAndroid from './NotificationAndroid'
import NotificationIos from './NotificationIos'

const debug = require('debug')('ditto:services:notifications:service')

class NotificationService {
  _notifications
  _state
  _loaded
  _isSyncing
  _action
  actions

  constructor () {
    if (isIos()) {
      this._notifications = new NotificationIos()
    } else {
      this._notifications = new NotificationAndroid()
    }
    this._state = new BehaviorSubject(null)
    this._loaded = new BehaviorSubject(false)
    this._isSyncing = false
    this._action = new Subject(null)
    this.actions = {
      ENABLE: 'notifications/enable',
      DISABLE: 'notifications/disable',
      DISPLAY: 'notifications/display'
    }
  }

  async init () {
    this._action.subscribe((action) => {
      switch (action.type) {
        case this.actions.ENABLE:
          this._enableNotifications()
          break
        case this.actions.DISABLE:
          this._disableNotifications()
          break
        case this.actions.DISPLAY:
          this._displayEventNotification(action.payload.eventId, action.payload.roomId)
          break
        default:
      }
    })

    auth.getAction().subscribe((action) => {
      switch (action.type) {
        case auth.actions.LOGGED_IN:
          this._startupNotifications()
          break
        case auth.actions.LOGOUT:
          this._cancelAllNotifications()
          this._shutdownNotifications()
          break
        default:
      }
    })

    chats.getAction().subscribe((action) => {
      switch (action.type) {
        case chats.actions.CHAT_OPENED:
          this._cancelNotification(action.payload)
          break
        default:
      }
    })

    this._isSyncing = true
    const jsonState = await database.getLocal('notifications')
    let state = JSON.parse(jsonState)
    if (state == null) state = { enabled: true }
    this._state.next(state)
    this._loaded.next(true)
    this._isSyncing = false
  }

  getAction () {
    return this._action
  }

  setAction (action) {
    if (!Object.values(this.actions).includes(action.type)) throw Error('Unknown action type:', action)
    this._action.next({ type: action.type, payload: action.payload })
  }

  getState () {
    return this._state
  }

  async setState (state) {
    if (this._isSyncing) debug('A database interaction is already running, ignoring…')
    this._isSyncing = true
    this._state.next(state)
    await database.setLocal('notifications', JSON.stringify(state))
    this._isSyncing = false
  }

  _cancelAllNotifications () {
    if (!isIos()) {
      this._notifications.cancelAllNotifications()
    }
  }

  _cancelNotification (roomId) {
    if (roomId == null || roomId === '') {
      throw Error('No roomId set')
    }
    if (!isIos()) {
      this._notifications.cancelNotification(roomId)
    }
  }

  async _disableNotifications () {
    await this._shutdownNotifications(true)
  }

  async _displayEventNotification (eventId, roomId) {
    if (!isIos()) {
      await this._notifications.displayEventNotification(eventId, roomId)
    }
  }

  async _enableNotifications () {
    await this._startupNotifications(true)
  }

  async _startupNotifications (enable = false) {
    const { enabled } = this._state.getValue()
    // On login, start notifications only if they are enabled
    if (!enabled && !enable) return

    const newState = { enabled: true }
    newState.pushkey = await this._notifications.startup()

    const pusherExists = await matrix.hasPusher(newState.pushkey)
    if (!pusherExists) {
      matrix.setPusher(newState.pushkey)
    }
    this.setState(newState)
  }

  _shutdownNotifications (disable = false) {
    this._notifications.shutdown()

    // On logout, remove the settings but keep enabled
    if (disable) {
      this.setState({ enabled: false })
    } else {
      this.setState({ enabled: true })
    }
  }

  // **********************************************
  // Helpers
  // **********************************************

  // Get the notification from which the app was opened if it was in the background or closed
  async getInitialNotification () {
    if (!isIos()) {
      return this._notifications.getInitialNotification()
    }
  }
}

const notificationService = new NotificationService()
export default notificationService
