import { IOverallAppState } from '../../store'
import { AppActionTypes } from './actions'

// const debug = require('debug')('ditto:services:app:reducer')

const initialState: IOverallAppState = {
  newChatModalVisible: false,
  theme: 'dark',
  pushNotifications: {
    enable: true,
    pushkey: null,
    fcmListener: null
  }
}

const AppReducer = (state = initialState, action) => {
  // debug('Action: ', action)
  switch (action.type) {
    case AppActionTypes.UPDATE_NEW_CHAT_MODAL_VISIBLE:
      return { ...state, newChatModalVisible: action.payload }
    case AppActionTypes.UPDATE_PUSH_NOTIFICATIONS:
      return { ...state, pushNotifications: action.payload }
    default:
      return state
  }
}
export default AppReducer
