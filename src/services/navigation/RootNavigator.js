import { createStackNavigator } from '@react-navigation/stack'
import { useObservableState } from 'observable-hooks'
import React from 'react'
import { Text } from 'react-native'

import auth from '../../scenes/auth/auth'
import AuthScreens from '../../scenes/auth/AuthScreens'
import MainNavigator from './MainNavigator'

// const debug = require('debug')('ditto:services:navigation:RootNavigator')

const Stack = createStackNavigator()

export default function RootNavigator ({ navigation }) {
  const authLoaded = useObservableState(auth.isLoaded())
  const authLoggedIn = useObservableState(auth.isLoggedIn())

  if (!authLoaded) {
    return (
      <Stack.Navigator headerMode='none'>
        <Stack.Screen name='Splash' component={Splash} />
      </Stack.Navigator>
    )
  } else if (authLoggedIn) {
    return (
      <Stack.Navigator headerMode='none'>
        <Stack.Screen name='App' component={MainNavigator} />
      </Stack.Navigator>
    )
  } else {
    return (
      <Stack.Navigator headerMode='none'>
        <Stack.Screen name='Auth' component={AuthScreens} />
      </Stack.Navigator>
    )
  }
}

const Splash = () => <Text>splash</Text>
