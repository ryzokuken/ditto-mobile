import { appSchema, Database as WatermelonDB } from '@nozbe/watermelondb'
import SQLiteAdapter from '@nozbe/watermelondb/adapters/sqlite'

import { MessageModel, messageSchema } from '../../scenes/chat/message/model'
import {
  ChatModel,
  chatSchema,
  MembershipModel,
  membershipSchema,
  ReceiptModel,
  receiptSchema
} from '../../scenes/chat/model'
import { UserModel, userSchema } from '../../scenes/user/model'

const schema = appSchema({
  version: 1,
  tables: [chatSchema, userSchema, messageSchema, membershipSchema, receiptSchema]
})

const adapter = new SQLiteAdapter({
  schema
})

class Database {
  _db

  constructor (options) {
    this._db = new WatermelonDB(options)
  }

  async getLocal (key) {
    return this._db.adapter.getLocal(key)
  }

  async setLocal (key, value) {
    return this._db.adapter.setLocal(key, value)
  }

  getCollection (collection) {
    return this._db.collections.get(collection)
  }

  async batch (actions) {
    return this._db.action(async (action) => this._db.batch(...actions))
  }
}

const database = new Database({
  adapter,
  modelClasses: [ChatModel, UserModel, MessageModel, MembershipModel, ReceiptModel],
  actionsEnabled: true
})
export default database
