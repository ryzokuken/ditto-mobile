import { BehaviorSubject } from 'rxjs'

import auth from '../../scenes/auth/service'
import messages from '../../scenes/chat/message/service'
import chats from '../../scenes/chat/service'
// import settings from '../../scenes/settings/service'
import users from '../../scenes/user/service'
import matrix from '../matrix/service'
import database from './database'

// TODO: matrix sdk memorystore, prefetch data
const debug = require('debug')('ditto:services:storage:service')

class StorageService {
  _hasFirstSync
  _hasQueue
  _isEmptying
  _isSyncing

  constructor () {
    this._hasFirstSync = new BehaviorSubject(false)
    this._hasQueue = false
    this._isEmptying = false
    this._isSyncing = false
  }

  init () {
    matrix.getAction().subscribe((action) => {
      switch (action.type) {
        case matrix.actions.CLIENT_SYNCING:
          if (!this._isEmptying) this._sync()
          break
        default:
      }
    })
    auth.getAction().subscribe((action) => {
      switch (action.type) {
        case auth.actions.LOGOUT:
          this._empty()
          this._hasFirstSync.next(false)
          break
        default:
      }
    })
    auth.init()
    // settings.init()
    chats.init()
    messages.init()
    users.init()
  }

  hasFirstSync () {
    return this._hasFirstSync
  }

  async _empty () {
    this._isEmptying = true
    const chatActions = await chats.getEmptyAllActions()
    const messageActions = await messages.getEmptyAllActions()
    const userActions = await users.getEmptyAllActions()
    const emptyActions = [...chatActions, ...messageActions, ...userActions]
    debug('Empty actions:', emptyActions)
    this._isEmptying = false

    try {
      await database.batch(emptyActions)
    } catch (e) {
      debug('Problem with database emptying:', e)
    }
  }

  _setSyncing (isSyncing) {
    this._isSyncing = isSyncing
    chats.setSyncing(isSyncing)
    messages.setSyncing(isSyncing)
    users.setSyncing(isSyncing)
  }

  async _sync () {
    if (this._isSyncing) {
      debug('Another sync is already running, queuing…')
      this._hasQueue = true
      return
    }

    this._setSyncing(true)
    const collections = {
      chats: {},
      messages: {},
      users: {},
      memberships: {},
      receipts: {}
    }

    const chatActions = await chats.getSyncActions(collections)
    const messageActions = await messages.getSyncActions(collections)
    const userActions = await users.getSyncActions(collections)
    debug('Sync collections:', collections)
    const syncActions = [...chatActions, ...messageActions, ...userActions]
    debug('Sync actions:', syncActions)

    try {
      await database.batch(syncActions)

      chats.finishSync()
      messages.finishSync()
      users.finishSync()
    } catch (e) {
      debug('Problem with database syncing:', e)
    } finally {
      this._setSyncing(false)
      if (!this._hasFirstSync.getValue() && matrix.isReady().getValue()) {
        this._hasFirstSync.next(true)
      }
      if (this._hasQueue) {
        chats.pushNextSyncList()
        messages.pushNextSyncList()
        users.pushNextSyncList()
        this._hasQueue = false
        this._sync()
      }
    }
  }
}

const storageService = new StorageService()
export default storageService
