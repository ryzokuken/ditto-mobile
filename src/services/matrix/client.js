import matrixSdk from 'matrix-js-sdk'
import request from 'xmlhttp-request'

const debug = require('debug')('ditto:services:matrix:client')

const MATRIX_CLIENT_CREATE_OPTIONS = {
  request: request,
  timelineSupport: true,
  unstableClientRelationAggregation: true
}

const MATRIX_CLIENT_START_OPTIONS = {
  initialSyncLimit: 8,
  lazyLoadMembers: true,
  pendingEventOrdering: 'detached'
}

class MatrixClient {
  _client

  constructor () {
    this._client = null
  }

  get () {
    return this._client
  }

  async login (username, password, homeserver) {
    try {
      let user = username
      let server = homeserver
      if (server.length === 0) {
        const splitUser = user.split(':')
        if (splitUser.length === 2) {
          user = splitUser[0]
          server = splitUser[1]
        } else server = 'matrix.org'
      }
      server = 'https://' + server
      debug('Logging in as %s on %s', user, server)
      const client = matrixSdk.createClient({
        baseUrl: server,
        ...MATRIX_CLIENT_CREATE_OPTIONS
      })
      const response = await client.loginWithPassword(user, password)
      this._client = client
      return {
        data: {
          userId: response.user_id,
          accessToken: response.access_token,
          homeserver: server,
          deviceId: response.device_id
        }
      }
    } catch (e) {
      debug('Error logging in:', e)
      let errorText = ''
      if (e.errcode) {
        switch (e.errcode) {
          case 'M_FORBIDDEN':
            errorText = 'Wrong username/password or homeserver'
            break
          case 'M_USER_DEACTIVATED':
            errorText = 'This user has been deactivated. Please contact your homeserver'
            break
          case 'M_LIMIT_EXCEEDED':
            errorText = 'Too many requests. Try again in a few minutes.'
            break
          default:
            errorText = 'An unknown error occured with the homeserver'
        }
      } else {
        errorText = 'The homeserver provided is not a Matrix homeserver or it is unreachable'
      }
      return {
        error: true,
        data: errorText
      }
    }
  }

  logout () {
    if (!this._client) {
      debug('logout: no client. We are probably already logged out.')
      return null
    }

    this._client.stopClient()
    delete this._client
  }

  create (userId, accessToken, homeserver) {
    if (!this._client) {
      this._client = matrixSdk.createClient({
        baseUrl: homeserver,
        userId,
        accessToken,
        ...MATRIX_CLIENT_CREATE_OPTIONS
      })
    }
  }

  start () {
    this._client.startClient(MATRIX_CLIENT_START_OPTIONS)
  }
}

const matrixClient = new MatrixClient()
export default matrixClient
