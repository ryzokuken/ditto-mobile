import React from 'react'
import { ActivityIndicator } from 'react-native'
import styled from 'styled-components/native'

import { COLORS } from '../constants'

// @flow

type ButtonProps = {
  title: string,
  children: any,
  disabled?: boolean,
  isLoading?: boolean,
  onPress: () => any
};

const ButtonWrapper = styled.TouchableOpacity`
  padding: 12px 40px;
  background-color: ${COLORS.blue};
  border-radius: 40;
  height: 55;
  min-width: 200;
  align-items: center;
  justify-content: center;
  opacity: ${({ disabled }) => (disabled ? 0.8 : 1)};

  box-shadow: 0px 5px 6px rgba(0, 0, 0, 0.15);
`

const ButtonText = styled.Text`
  color: ${({ theme }) => theme.primaryTextColor};
  font-size: 24;
  font-weight: bold;
`

const Button = ({
  title = 'New Button',
  onPress = () => {},
  children,
  disabled = false,
  isLoading = false,
  ...props
}: ButtonProps) => (
  <ButtonWrapper
    disabled={disabled || isLoading}
    onPress={() => {
      if (!disabled && !isLoading) onPress()
    }}
    {...props}
  >
    {isLoading ? (
      <ActivityIndicator color='#fff' size='large' />
    ) : (
      <ButtonText>{title}</ButtonText>
    )}
  </ButtonWrapper>
)
export default Button
