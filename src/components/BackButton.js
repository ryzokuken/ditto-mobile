import * as React from 'react'
import Icon from 'react-native-vector-icons/Feather'

import theme from '../theme'
import HeaderButton from './HeaderButton'

const BackButton = ({ onPress, style, size }) => (
  <HeaderButton style={style} onPress={onPress}>
    <Icon
      name='chevron-left'
      size={size || 35}
      color={theme.iconColor}
      style={{ marginLeft: 4 }}
    />
  </HeaderButton>
)
export default BackButton
