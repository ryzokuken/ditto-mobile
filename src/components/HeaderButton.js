import styled from 'styled-components/native'

const HeaderButton = styled.TouchableOpacity`
  width: 40;
  height: 40;
  margin-right: 6;
  justify-content: center;
  align-items: center;
`
export default HeaderButton
