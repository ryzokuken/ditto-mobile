import styled from 'styled-components/native'

const Input = styled.TextInput`
  border-radius: 10;
  /* background-color: ${({ theme }) => theme.darkAccentColor};
  color: ${({ theme }) => theme.primaryTextColor}; */
  font-size: 22;
  height: 55;
  padding-right: 15;
  padding-left: 15;
  min-width: 200;
  margin: ${({ marginValue }) => (marginValue || '0')};
`
export default Input
