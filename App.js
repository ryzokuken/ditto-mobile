import 'react-native-gesture-handler'

import { dark as darkTheme, mapping } from '@eva-design/eva'
import { NavigationContainer } from '@react-navigation/native'
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components'
import { EvaIconsPack } from '@ui-kitten/eva-icons'
import React from 'react'

import RootNavigator from './src/services/navigation/RootNavigator'
import { navigationRef, routeRef } from './src/services/navigation/service'
import dittoDarkTheme from './src/themes/dittoDark.json' // <-- Import app theme
import { ThemeContext } from './src/themes/ThemeContext'

const themes = {
  dittoDark: { ...darkTheme, ...dittoDarkTheme }
}

// import * as Sentry from '@sentry/react-native';
// Sentry.init({
//   dsn: 'https://42044a4916614554874cdaf96ea70e59@sentry.io/1529001',
// });

const debug = require('debug')
debug.enable('ditto:*')

console.disableYellowBox = true

const App = () => {
  const [theme, setTheme] = React.useState('dittoDark')
  const currentTheme = themes[theme]

  const getActiveRoute = state => {
    const route = state.routes[state.index]
    if (route.state) {
      // Dive into nested navigators
      return getActiveRoute(route.state)
    }
    return route
  }

  const toggleTheme = (nextTheme: 'light' | 'dark' | 'dittoDark') => {
    setTheme(nextTheme)
  }

  return (
    <>
      <IconRegistry icons={EvaIconsPack} />
      <ThemeContext.Provider value={{ theme, toggleTheme }}>
        <ApplicationProvider mapping={mapping} theme={currentTheme}>
          <NavigationContainer
            ref={navigationRef}
            onStateChange={state => { routeRef.current = getActiveRoute(state) }}
          >
            <RootNavigator />
          </NavigationContainer>
        </ApplicationProvider>
      </ThemeContext.Provider>
    </>
  )
}

export default App
